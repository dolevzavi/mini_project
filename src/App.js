import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import TeamsTable from './components/TeamsTable';
import CreateTeamPage from './components/CreateTeamPage';
import EditTeamPage from './components/EditTeamPage';
import TeamSideBar from './components/TeamSideBar';
import TeamMain from './components/TeamMain';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>

          <Route exact path='/home'>
            <TeamsTable />
          </Route>

          <Route exact path='/create_team'>
            <CreateTeamPage />
          </Route>
          
          <Route exact path='/edit_team/:teamId'>
            <EditTeamPage />
          </Route>

          <Route path='*'>
            <p>page not found</p>
          </Route>


        </Switch>
      </Router>
    </div>
  );
}

export default App;
