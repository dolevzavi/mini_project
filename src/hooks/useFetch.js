import { useState, useEffect } from "react"


const useFetch = (url) => {

    const [data, setData] = useState(null)
    const [isPending, setIsPending] = useState(true)
    
    useEffect(() => {

      setTimeout(() => {
        fetch(url)
        .then(res => {
          if (!res.ok) {
            throw Error('error: could not fetch the data')
          }
          return res.json()
        })
        .then(
            data => setData(data),
        ).then(
          isPending => setIsPending(false)
        ).catch(err => {
          console.log(err.message) 
        })
      }, 500)
      
    }, [url])

    return { data, setData, isPending }
}

export default useFetch;