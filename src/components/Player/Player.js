import useFetch from "../../hooks/useFetch"


const Player = (props) => {

    const playerId = props.playerId
    
    const playerData = useFetch('get_player_info')

    return (
        <div className="player-component">
            <p>this is a player with id of {playerId}</p>
        </div>
    )
}

export default Player;