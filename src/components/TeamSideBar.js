const TeamSideBar = (props) => {
    
    const setCurrentSubPage = props.setCurrentSubPage;
    const currentSubPage = props.currentSubPage;

    const sidebarButtonsNames = ['formation', 'players', 'more']

    return (
      <div className="team-side-bar-component">
        <div className="sidebar">

          {sidebarButtonsNames.map((buttonName) => {
            if (buttonName === currentSubPage) {
              return <button className="sidebar-button sidebar-button-selected" onClick={() => setCurrentSubPage(buttonName)} key={buttonName}>{buttonName}</button>
            }
            else {
              return <button className="sidebar-button" onClick={() => setCurrentSubPage(buttonName)} key={buttonName}>{buttonName}</button>
            }
          })}
        </div>
      </div>
    )
}

export default TeamSideBar;