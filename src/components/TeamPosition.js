import { useState } from "react";
import Player from "./Player/Player";


const TeamPosition = (props) => {
    
    const teamData = props.teamData;
    console.log('running')

    const [playersList, setPlayersList] = useState(teamData.players_list);

    const position = props.position;

    return (
        <div className="team-position-component">
            <div className="team-position-container">
                
                <div className="team-position-header">
                    <p>{position}</p>

                </div>

                <div className="team-position-players">
                    {playersList.map((playerId) => {
                        return (
                          <div className="team-position-players-list" key={position}>
                            <Player playerId={playerId} />
                            <Player playerId={playerId} />
                          </div>
                        )
                    })}
                    
                    <div className="simple-button-container">
                        <button className="simple-button">+</button>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default TeamPosition;