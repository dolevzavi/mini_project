import { useState, useEffect } from "react";
import TeamSideBar from "./TeamSideBar";
import TeamForm from "./TeamForm";

import useFetch from "../hooks/useFetch";

import FormationContent from './teamSubPages/FormationContent'
import PlayersContent from './teamSubPages/PlayersContent'
import MoreContent from './teamSubPages/MoreContent'

const TeamMain = (props) => {    
    
    const getContent = (currentPage) => {
        if (currentPage === 'formation') {
            return <FormationContent teamData={teamData}/>
        }
        else if (currentPage === 'players') {
            return <PlayersContent teamData={teamData}/>
        }
        else if (currentPage === 'more') {
            return <MoreContent teamData={teamData}/>
        }
    }

    const submitAction = props.submitAction;
    
    const [currentSubPage, setCurrentSubPage] = useState('formation')
    
    const teamId = props.teamId;
    const {data: teamData, setData, isPending} = useFetch('/get_team_data/'+teamId)

    return (
        <div className="team-main-component">
          {!isPending && <TeamSideBar currentSubPage={currentSubPage} setCurrentSubPage={setCurrentSubPage} />}
          {!isPending && <TeamForm submitAction={submitAction} subPageContent={getContent(currentSubPage)} teamData={teamData} setTeamData={setData} teamId={teamId} />}            
        </div>
    )
}

export default TeamMain;