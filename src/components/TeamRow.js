const TeamRow = (props) => {

    const teamName = props.team.name;
    const teamRating = props.team.rating;
    const teamId = props.team.team_id;

    return (
      <div className="team-row-component">
        <div className="team-row">
          <div className="team-row-data">
            <div className="team-row-name">
                <p>{teamName}</p>
            </div>
            <div className="team-row-rating">
              <p>{teamRating}</p>
            </div>
          </div>

          <div className="team-row-buttons">
            <div className="simple-button-container">
                <button className="team-row-edit-button" onClick={() => props.handleClickFunction('edit_team', teamId)}>edit</button>
            </div>
            <div className="simple-button-container">
              <button className="team-row-delete-button" onClick={() => props.handleClickFunction('show_delete_modal', teamId)}>x</button>
            </div>
          </div>
        </div>
      </div>
    )
}

export default TeamRow;