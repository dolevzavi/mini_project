import TeamPosition from "../TeamPosition";


const PlayersContent = (props) => {
    
    const teamData = props.teamData;

    return (
      <div className="more-content-componenet">
            <TeamPosition position='goalkeeper' teamData={teamData} />
            <TeamPosition position='defenders' teamData={teamData} />
            <TeamPosition position='middlefielders' teamData={teamData} />
            <TeamPosition position='attackers' teamData={teamData} />
      </div>
    )
}

export default PlayersContent;