import { useState } from "react";


const FormationContent = (props) => {
    
    const teamData = props.teamData;

    const formations = [
        {'id': 1, 'name': '4-3-3', 'gk': 1, 'def': 4, 'mid': 3, 'atk': 3},
        {'id': 2, 'name': '4-4-2', 'gk': 1, 'def': 4, 'mid': 4, 'atk': 2},
        {'id': 3, 'name': '5-3-2', 'gk': 1, 'def': 5, 'mid': 3, 'atk': 2},
        {'id': 4, 'name': '4-2-4', 'gk': 1, 'def': 4, 'mid': 2, 'atk': 4},
        {'id': 5, 'name': '3-4-3', 'gk': 1, 'def': 3, 'mid': 4, 'atk': 3},
        {'id': 6, 'name': '5-4-1', 'gk': 1, 'def': 5, 'mid': 4, 'atk': 1},
        {'id': 7, 'name': '4-5-1', 'gk': 1, 'def': 4, 'mid': 5, 'atk': 1},
        {'id': 8, 'name': '5-2-3', 'gk': 1, 'def': 5, 'mid': 2, 'atk': 3},
    ]


    const [selectedFormation, setSelectedFormation] = useState(null)
    let formationClass = 'formation'


    
    return (
        <div className="formation-content-componenet">
            {formations.map((formation) => {
              if (formation.id === selectedFormation) {
                formationClass = 'formation formation-selected'
              }
              else {
                 formationClass = 'formation'
              }
              return (
                <div className={formationClass} key={formation.name} onClick={() => setSelectedFormation(formation.id)}>
                  <div className="center-container"><p className="formation-header">{formation.name}</p></div>
                  <div className="formation-contents-container"> 
                    <p className="formation-content">{formation.gk} goalkeepr</p>
                    <p className="formation-content">{formation.def} defenders</p>
                    <p className="formation-content">{formation.mid} middlefielders</p>
                    <p className="formation-content">{formation.atk} attackers</p>
                  </div>
                </div>
              )
            })}
        </div>
    )
}

export default FormationContent;