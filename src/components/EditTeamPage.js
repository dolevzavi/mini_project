import TeamMain from "./TeamMain";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";


const EditTeamPage = () => {

    const { teamId } = useParams()

    return (
      <div className="edit-team-page-component">
        <TeamMain submitAction='edit' teamId={teamId} />
      </div>
    )
}

export default EditTeamPage;