import { useHistory, useParams } from "react-router-dom"
import useFetch from "../hooks/useFetch"
import { useState } from "react"


const TeamForm = (props) => {

  const history = useHistory()

    const teamId = props.teamId
    
    // props got from TeamMain component
    const teamData = props.teamData
    const setTeamData = props.setTeamData
    const subPageContent = props.subPageContent

    const submitAction = props.submitAction // can be 'create' or 'edit'

    const handleSubmit = (e, submitAction) => {
        e.preventDefault()
        
        let url;
        if (submitAction === 'create') {
            url = '/create_team'
        }
        else if (submitAction === 'edit') {
            url = '/edit_team/' + teamId
        }

        fetch(url, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({'team_name': teamData.name})
        })
        history.push('/home')
    }

    const changeTeamName = (teamName) => {
        const updatedTeamData = { ...teamData, name: teamName };
        setTeamData(updatedTeamData);
    };
    
    return (
      <div className="team-page-component">

        <form className="create-team-form" onSubmit={(e) => handleSubmit(e, submitAction)}>
            
          <div className="center-container">
            <p className="form-header">Name</p>
            <input
             className="simple-text-input team-name-text-input"
             type="text"
             required
             value={teamData.name} 
             onChange={(e) => changeTeamName(e.target.value)} />
          </div>

          {subPageContent && subPageContent}
    
          <div className="submit-container">
            <button className="simple-button">submit</button>
          </div>

        </form>

      </div>
    )
}

export default TeamForm;