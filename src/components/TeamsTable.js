import TeamRow from "./TeamRow"
import useFetch from '../hooks/useFetch'
import { useState } from "react"
import DeleteTeamModal from "./modals/DeleteTeamModal";
import { useHistory } from "react-router-dom";


const TeamsTable = () => {

    const history = useHistory()

    let {data: teamsList, setData, isPending} = useFetch('/get_teams_list')

    const [isDeleteModal, setIsDeleteModal] = useState(false)
    const [deleteModalTeamId, setDeleteModalTeamId] = useState(null)


    const handleClick = (action, teamId) => {
        if (action === 'show_delete_modal') {
            setDeleteModalTeamId(teamId)
            setIsDeleteModal(true)
        }
        else if(action === 'create_new_team') {
          history.push('/create_team/')
        }
        else if(action === 'edit_team') {
          history.push('/edit_team/'+teamId)
        }
    }

    const removeTeamFromList = (teamId) => {
      let updatedList = teamsList.filter(item => item.team_id !== teamId)
      setData(updatedList)
    }

    return (
        <div className="teams-table-component">
          <div className="teams-table">
            <div className="create-team-button-container">
              <button className="simple-button create-team-button" onClick={() => handleClick('create_new_team')}>create new team</button>
            </div>

            <div className="teams-table-header">
              <p>Teams:</p>
            </div>
            {isPending && <p>loading...</p>}
            {teamsList && teamsList.map((team) => (
              <TeamRow team={team} handleClickFunction={handleClick} key={team.team_id}/>
            ))}
          </div>

          {isDeleteModal && <DeleteTeamModal teamId={deleteModalTeamId} setIsDeleteModal={setIsDeleteModal} removeTeamFromList={removeTeamFromList}/>}
        </div>
    )
}

export default TeamsTable;