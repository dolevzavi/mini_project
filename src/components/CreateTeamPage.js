import TeamMain from "./TeamMain";


const CreateTeamPage = () => {
    
    return (
      <div className="create-team-page-component">
        <TeamMain submitAction='create' />
      </div>
    )
}

export default CreateTeamPage;