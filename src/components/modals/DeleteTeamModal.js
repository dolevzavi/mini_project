const DeleteTeamModal = (props) => {

    const teamId = props.teamId;
    console.log('the modal is now in: ', teamId)

    const handleClick = (action, teamId) => {
        if (action === 'delete') {
            fetch('/delete_team/'+teamId)

            props.removeTeamFromList(teamId)
        }
        props.setIsDeleteModal(null);
    }

    return (
        <div className="delete-team-modal-component">
          <div className="overlay">
            <div className="simple-modal team-row-delete-modal">
              <div className="simple-modal-message">
                <p>are you sure you want to delete this team?</p>
              </div>
              <div className="simple-modal-buttons-container">
                <div className="simple-button-container">
                  <button className="simple-button delete-team-modal-delete" onClick={() => handleClick('delete', teamId)}>delete</button>
                </div>
                <div className="simple-button-container">
                  <button className="simple-button" onClick={() => handleClick('cancel')}>cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
}

export default DeleteTeamModal;