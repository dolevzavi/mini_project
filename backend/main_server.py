from flask import Flask, jsonify, request
import functions as funcs


app = Flask(__name__)
app.secret_key = 'doesnt matter what the secret key is'

funcs.db_get_teams()


@app.route('/get_teams_list', methods=['GET'])
def get_teams():
    return jsonify(funcs.db_get_teams())
   

@app.route('/delete_team/<team_id>', methods=['GET', 'POST'])
def delete_team(team_id):
    funcs.db_delete_team(team_id)
    return ''


@app.route('/create_team', methods=['GET', 'POST'])
def create_team():
    if request.method == 'POST':
        team_data = request.get_json()
        funcs.db_create_team(team_data)

        return ''
    return ''


@app.route('/get_team_data/<team_id>')
def get_team_data(team_id):
    team_data = funcs.db_get_team_data(team_id)
    return jsonify(team_data)


@app.route('/edit_team/<team_id>', methods=['GET', 'POST'])
def edit_team(team_id):
    if request.method == 'POST':
        updated_team_data = request.get_json()
        funcs.db_edit_team(team_id, updated_team_data)
        return ''
    return ''


if __name__ == '__main__':
    app.run(debug=True)
    
