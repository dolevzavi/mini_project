from pymongo import MongoClient

connection_string = r'mongodb://localhost:27017'
client = MongoClient(connection_string)
db = client.mini_project


def db_get_teams():
    teams_list = list(db.teams.find({}, {"_id": 0}))
    
    return teams_list
    

def db_delete_team(team_id):
    team_to_delete = db.teams.delete_one({'team_id': int(team_id)})


def db_create_team(team_data):
    collection = db.teams
    created_team = {
        'name': team_data['team_name'],
        'rating': -1,
        'team_id': generate_team_id(collection)
    }    
    collection.insert_one(created_team)


def db_get_team_data(team_id):
    team_data = db.teams.find_one({'team_id': int(team_id)}, {'_id': 0})
    
    if team_data is None:  
        team_data = {'name': '', 'rating': 0}

    return team_data


def db_edit_team(team_id, updated_data):
    db.teams.update_one({'team_id': int(team_id)}, {'$set': {'name': updated_data['team_name']}})    


def generate_team_id(collection):
    teams_ids = [team['team_id'] for team in list(collection.find({}, {'team_id': 1}))]
    max_id = max(teams_ids)
    return max_id + 1